import re
fobjr=open("hosts.real","r")
fobjw=open("hosts2.real","w")
fobjw2=open("hosts3.real", "w")

for line in fobjr:
    word = line.split("#")
    fobjw.write(word[0])
    if len(word) > 1:
        fobjw.write("\n")

    remac=re.compile("(?:[0-9a-fA-F]:?){12}")
    mac=remac.search(line)
    if mac != None:
        print(mac.group(0))
    print("\n")

    reip=re.compile("([0-9]{1,3}\.){3}[0-9]{1,3}")
    ip=reip.search(line)
    if ip != None:
        fobjw2.write(ip.group(0))
    fobjw2.write("\n")

fobjr.close()
fobjw.close()
