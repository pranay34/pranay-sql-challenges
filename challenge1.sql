delimiter //
create procedure vwap(input1 varchar(20))
begin
	-- Set User Input to @starttime
	set @starttime = input1;

    -- VWAP Calculation
    select cast(sum(`<vol>`*`<close>`)/sum(`<vol>`) as decimal(11,8)) as VWAP,

    -- Convert Date as string to Date object
    date_format(str_to_date(@starttime, "%Y%m%d"), "%d/%m/%Y") as `Date`,

    -- Print out the overall 5 our interval
    concat("Start: ", time_format(str_to_date(@starttime, "%Y%m%d%H%i"), "%H:%i"), " & End: ",
    time_format(date_add(str_to_date(@starttime,"%Y%m%d%H%i"), interval 5 hour), "%H:%i")) as `Interval`
    from sd2

    -- Ensures data is gathered from defined 5 hour period
    where `<date>` between @starttime and date_format(date_add(str_to_date(@starttime,"%Y%m%d%H%i"), interval 5 hour), "%Y%m%d%H%i");
end //
delimiter ;
