# Pranay Mistri - SQL Challenges


# Challenge 1 - Volume Weighted Price

No SQL was used to change the column headings. Angular brackets remained in use.

## Assumptions:

> Trade_Price was taken as the close price.

> Trade_Size was taken as volume.

## Script File:

```bash
challenge1.sql
```

## Calling the procedure:

```mysql
call vwap(201010111500);
```

## Example output:

Starting at 15:00

```bash
 VWAP,           Date,         Interval
'295.68702430', '11/10/2010', 'Start: 15:00 & End: 20:00'
```
# Challenge 2 - Biggest Range

## Temporary Table names:

> temptable1

> temptable2

> temptable3

## Script File:

```bash
challenge2.sql
```

## Calling the procedure:

```mysql
call biggestRange();
```

## Example output:

```bash
 Date,         Range,    Time of Max Price
'06/08/2006', '0.2900', '0947'
'06/13/2006', '0.2800', '0952'
'06/13/2006', '0.2800', '0953'
'06/14/2006', '0.2700', '0954'
```
