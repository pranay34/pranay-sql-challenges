import re

file = open("opra_example_regression.log", "r")

record_publish_re = re.compile("Record Publish")
type_trade_re = re.compile("Type: Trade")
trade_price_re = re.compile("wTradePrice")
trade_volume_re = re.compile("wTradeVolume")

record_publish = trade_price = trade_volume = type_trade = ""
for line in file:
    if "Record Publish" in line:
        record_publish = line
        trade_price = trade_volume = type_trade = ""
    if "Type: Trade" in line:
        type_trade = line

    if record_publish and type_trade:
            if "wTradePrice" in line:
                trade_price = line
            if "wTradeVolume" in line:
                trade_volume = line
            if trade_price and trade_volume:
                record_publish=re.sub("^Regression:\s+", "",record_publish)
                trade_price=re.sub("^Regression:\s+", "",trade_price)
                trade_volume=re.sub("^Regression:\s+", "",trade_volume)
                type_trade=re.sub("^Regression:\s+", "",type_trade)
                print(record_publish.strip() + "  " + type_trade.strip())
                print(trade_price.strip())
                print(trade_volume.strip())
                record_publish = trade_price = trade_volume = type_trade = ""
