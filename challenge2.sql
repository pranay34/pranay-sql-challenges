delimiter //
create procedure biggestRange()
begin
-- Temp table 1: Fetch date and calculate the 3 greatest range values
create temporary table temptable1
select `Date`, cast(max(abs(`Open`-`Close`)) as decimal(6,2)) as `Range` from sd3
group by `Date`
order by 2
desc limit 3;

-- select * from temptable1;

-- Temp table 2: Fetch date equiv to temptable1, whilst outputting the value of the high price
create temporary table temptable2
select `Date`, max(`High`) as `maxHigh` from sd3
where `Date` in
	(
		select `Date`
        from temptable1
        where temptable1.`Date` = sd3.`Date`
	)
group by `Date`
order by 2
desc limit 3;

-- select * from temptable2;

-- Temp table 3: Create table which has date and time of when high value occurred - relates to previous tables
create temporary table temptable3
select sd3.`Date`, `Time` from sd3
join temptable2 on sd3.`Date`=temptable2.`Date`
where temptable2.`maxHigh` = sd3.`High`;

-- select * from temptable3;

-- Table 4: Final output table, which merges first temp table and third. Prints date, range and time of max price
-- create table output_table
select temptable3.`Date`, temptable1.`Range`, temptable3.`Time` as `Time of Max Price` from temptable1, temptable3
where temptable1.`Date` = temptable3.`Date`;

-- Execute line for final output
-- select * from output_table;

-- Drop tables
drop table temptable1;
drop table temptable2;
drop table temptable3;
-- drop table output_table;
end //
delimiter ;
